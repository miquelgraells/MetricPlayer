package com.test.npaw.metriclibrary.HttpClient;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MetricLibraryService {
    @GET("search?")
    Call<Void> startMessage(@Query("param") String param);

    @GET("search?")
    Call<Void> frameMessage(@Query("param") String param);

    @GET("search?")
    Call<Void> finishMessage(@Query("param") String param);
}
