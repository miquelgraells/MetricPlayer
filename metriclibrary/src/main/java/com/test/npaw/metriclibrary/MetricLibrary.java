package com.test.npaw.metriclibrary;


import android.content.Context;


import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.video.VideoListener;
import com.test.npaw.metriclibrary.HttpClient.VideoFinishedRequest;
import com.test.npaw.metriclibrary.HttpClient.VideoFrameRequest;
import com.test.npaw.metriclibrary.HttpClient.VideoStartsLoadRequest;

public class MetricLibrary implements Player.EventListener, VideoListener {

    Context context;
    MetricLibrayInterface metricLibrayInterface;
    int timesStarted = 0;
    int timesPause = -1;
    long startCounting = 0;
    boolean videoLoaded = false;
    boolean firstFrameLoaded = false;

    public MetricLibrary(Context context, MetricLibrayInterface metricLibrayInterface) {
        this.context = context;
        this.metricLibrayInterface = metricLibrayInterface;
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        if (isLoading && !videoLoaded) {
            videoLoaded = true;
            VideoStartsLoadRequest videoStartsLoadRequest = new VideoStartsLoadRequest(context,"param");
            videoStartsLoadRequest.doRequest();
        }
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playWhenReady && playbackState == Player.STATE_READY) { // on play video
            metricLibrayInterface.onStartedTimesUpdated(++timesStarted);

            if (timesPause > 0) { // video paused at least once
                long now = System.currentTimeMillis();
                metricLibrayInterface.timeElapsed(now - startCounting);
                // start counting restart to 0 when user plays the video again
                startCounting = 0;
            }
        } else if (playbackState == Player.STATE_ENDED) {
            metricLibrayInterface.onVideoEnded();
            VideoFinishedRequest videoFinishedRequest = new VideoFinishedRequest(context,"param");
            videoFinishedRequest.doRequest();
        } else { // on pause video
            metricLibrayInterface.onPausedTimesUpdated(++timesPause);

            if (timesPause > 0) {
                startCounting = System.currentTimeMillis();
            }
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {

    }

    @Override
    public void onRenderedFirstFrame() {
        if (!firstFrameLoaded) {
            firstFrameLoaded = true;
            VideoFrameRequest videoFrameRequest = new VideoFrameRequest(context, "param");
            videoFrameRequest.doRequest();
        }
    }

    public interface MetricLibrayInterface {
        void onStartedTimesUpdated(int times);
        void onPausedTimesUpdated(int times);
        void timeElapsed(long time);
        void onVideoEnded();
    }
}
