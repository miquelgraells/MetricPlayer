package com.test.npaw.metriclibrary.HttpClient;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.test.npaw.metriclibrary.MetricLibrary;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class VideoStartsLoadRequest extends BaseRequest implements Callback<Void> {

    MetricLibraryService service;
    String param;
    public VideoStartsLoadRequest(Context context, String param) {
        super(context);
        this.param = param;
        service = retrofit.create(MetricLibraryService.class);
    }

    @Override
    public void doRequest() {
        Call<Void> call = service.startMessage(param);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Void> call, Response<Void> response) {
        Log.d(TAG, "onResponse: ");
    }

    @Override
    public void onFailure(Call<Void> call, Throwable t) {
        Log.d(TAG, "onFailure: ");
    }
}
