package com.test.npaw.metriclibrary.HttpClient;

import android.content.Context;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * All request must extends this class.
 * This class initizialises retrofit and okhttp
 */
public abstract class BaseRequest {

    Retrofit retrofit;

    public BaseRequest(Context context) {
        OkHttpClient okHttpClient = provideOkHttpClient(context);

        retrofit = new Retrofit.Builder()
                .baseUrl("http://www.google.es")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private OkHttpClient provideOkHttpClient(Context context) {
        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS);
        return okhttpClientBuilder.build();
    }

    public abstract void doRequest();
}