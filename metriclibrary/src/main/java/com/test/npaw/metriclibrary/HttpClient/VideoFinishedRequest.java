package com.test.npaw.metriclibrary.HttpClient;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class VideoFinishedRequest extends BaseRequest implements Callback<Void> {

    MetricLibraryService service;
    String param;

    public VideoFinishedRequest(Context context, String param) {
        super(context);
        this.param = param;
        service = retrofit.create(MetricLibraryService.class);
    }

    @Override
    public void doRequest() {
        Call<Void> call = service.finishMessage(param);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Void> call, Response<Void> response) {
        Log.d(TAG, "onResponse: ");
    }

    @Override
    public void onFailure(Call<Void> call, Throwable t) {
        Log.d(TAG, "onFailure: ");
    }
}
