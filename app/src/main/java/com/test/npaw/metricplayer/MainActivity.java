package com.test.npaw.metricplayer;

import android.content.DialogInterface;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.test.npaw.metriclibrary.MetricLibrary;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements MetricLibrary.MetricLibrayInterface, Player.EventListener {

    static final String MP4_VIDEO_URI = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";

    TextView playsTV, pausesTV, timeTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PlayerView playerView = findViewById(R.id.player);
        playsTV = findViewById(R.id.plays);
        pausesTV = findViewById(R.id.pauses);
        timeTV = findViewById(R.id.time);

        playsTV.setText("plays : 0");
        pausesTV.setText("pauses : 0");
        timeTV.setText("seconds : 0");

        // 1. Create a default TrackSelector
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        // 2. Create the player
        SimpleExoPlayer player =
                ExoPlayerFactory.newSimpleInstance(this, trackSelector);

        playerView.setPlayer(player);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "yourApplicationName"), bandwidthMeter);


        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(MP4_VIDEO_URI));
        // Prepare the player with the source.
        player.prepare(videoSource);

        // Instanciate module indicating this activity as listener
        MetricLibrary metricLibrary = new MetricLibrary(this,this);
        // passing module as listener to ExoPlayer
        player.addListener(metricLibrary);
        player.addVideoListener(metricLibrary);

        // to check that MetricLibrary dont interfere with listeners
        player.addListener(this);
    }

    @Override
    public void onStartedTimesUpdated(int times) {
        playsTV.setText("plays : " + times);
    }

    @Override
    public void onPausedTimesUpdated(int times) {
        pausesTV.setText("paused : " + times);
    }

    @Override
    public void timeElapsed(long time) {
        time = TimeUnit.MILLISECONDS.toSeconds(time);
        timeTV.setText("seconds : " + String.valueOf(time));
    }

    @Override
    public void onVideoEnded() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.video_finished))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        builder.create().show();
    }

    //-------- to check that MetricLibrary dont interfere with listeners
    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        Toast toast = Toast.makeText(this, "playWhenReady " + String.valueOf(playWhenReady), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }
}
