# EXOPLAYER PLUGIN MetricLibray


## How to use the plugin MetricLibray

```
// Create the player
   SimpleExoPlayer player =  ExoPlayerFactory.newSimpleInstance(this, trackSelector);

// Instanciate the module passing as first paramater the context and sencond the listeining interface of MetricLibrayInterface
   MetricLibrary metricLibrary = new MetricLibrary(this,this);

// passing the module as listener to ExoPlayer
   player.addListener(metricLibrary);
   player.addVideoListener(metricLibrary);

```

### Override the next methods related to MetricLibrayInterface

It is launched the each time user presses the play button, given as paramater the number of times user has done this action.
	
```
    @Override
    public void onStartedTimesUpdated(int times) {
        
    }

```

It is launched the each time user presses the pause button pause, given as paramater the number of times user has done this action.
```
    @Override
    public void onPausedTimesUpdated(int times) {
       
    }

```
It is launched the each time user presses the button play after hab been presed paused button, given as paramater the time passed in milliseconds.

```
    @Override
    public void timeElapsed(long time) {
       
    }

```
It is launched when video finished.
```
    @Override
    public void onVideoEnded() {

    }
```
